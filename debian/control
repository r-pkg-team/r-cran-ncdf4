Source: r-cran-ncdf4
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-ncdf4
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-ncdf4.git
Homepage: https://cran.r-project.org/package=ncdf4
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               libnetcdf-dev,
               netcdf-bin
Testsuite: autopkgtest-pkg-r

Package: r-cran-ncdf4
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R interface to Unidata netCDF format data files
 High-level R interface to data files written using Unidata's netCDF
 library (version 4 or earlier), which are binary data files that are
 portable across platforms and include metadata information in addition
 to the data sets. Using this package, netCDF files
 (either version 4 or "classic" version 3) can be opened and data sets
 read in easily. It is also easy to create new netCDF dimensions,
 variables, and files, in either version 3 or 4 format, and manipulate
 existing netCDF files.
